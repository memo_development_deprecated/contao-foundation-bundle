<?php

declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\FoundationBundle\Model;

use Contao\Config;
use Contao\Controller;
use Contao\Database;
use Contao\Model\Collection;
use Contao\System;
use Contao\ModuleModel;
use Memo\FoundationBundle\Service\ToolboxService;
use Memo\ProductBundle\Model\ProductAttributeValueModel;
use Memo\ProductBundle\Model\ProductModel;

/**
 * Class FoundationModel
 *
 * Enable ease Translation of all fields in DCA
 */
class FoundationAttributeModel extends FoundationModel
{

    public static function resolveFilterForm($arrFormData)
    {

        // Get all attributes
        $arrColumns = array();
        $arrValues = array();
        $arrAttributes = array();
        if ($colAttributes = self::findAll()) {
            foreach ($colAttributes as $objAttribute) {
                $arrAttributes[$objAttribute->alias] = $objAttribute;
            }
        }

        // Add the form filters
        if (is_array($arrFormData) && count($arrFormData) > 0) {
            foreach ($arrFormData as $strKey => $strValue) {
                if (array_key_exists(str_replace('_from', '', $strKey), $arrAttributes)) {
                    $strKey = str_replace('_from', '', $strKey);
                }

                if (!array_key_exists($strKey, $arrAttributes) && !array_key_exists(str_replace('_from', '', $strKey), $arrAttributes)) {
                    continue;
                }

                if ($strValue == '') {
                    continue;
                }

                $objAttribute = $arrAttributes[$strKey];
                $strDatebaseField = $objAttribute->getFieldName();

                if ($objAttribute->range) {
                    $strFieldFrom = $strKey . '_from';
                    $strDatebaseFieldFrom = $strDatebaseField . '_from';
                    $strFieldTo = $strKey . '_to';
                    $strDatebaseFieldTo = $strDatebaseField . '_to';
                    $intValueFrom = $arrFormData[$strFieldFrom];
                    $intValueTo = $arrFormData[$strFieldTo];

                    if (is_numeric($intValueFrom) && is_numeric($intValueTo)) {

                        // Case 1 - if min value is larger than input min and smaller than the max input value
                        // Case 2 - if max value is larger then input min and smaller than the max input value
                        // Case 3 - if the input min is larger than the min value and the input max is smaller than the max value

                        $arrColumns[] = "(($strDatebaseFieldFrom >= $intValueFrom AND $strDatebaseFieldFrom <= $intValueTo) OR
                        ($strDatebaseFieldTo >= $intValueFrom AND $strDatebaseFieldTo <= $intValueTo) OR
                        ($strDatebaseFieldFrom <= $intValueFrom AND $strDatebaseFieldTo >= $intValueTo))";
                    }

                } elseif ($objAttribute->multiple) {
                    $arrColumns[] = '(' . $strDatebaseField . ' LIKE \'%%:"' . $strValue . '";%%\' OR ' . $strDatebaseField . ' LIKE \'%%:' . $strValue . ';%%\')';
                } else {
                    $arrColumns[] = $strDatebaseField . '=?';
                    $arrValues[] = $strValue;
                }


            }
        }
        return array($arrColumns, $arrValues);
    }

    public function getOptions($bolOnlyActive = false, $colItems = false, $bolAddBlankOption = true, $bolValues=false)
    {
        // Get the Attribute Model Class
        $strAttributeClass = get_class($this);

        // Generate the attribute value class name
        $strAttributeValueClass = preg_replace('/Model$/', 'ValueModel', $strAttributeClass);

        // Get Table from Item Class
        $strAttributeValueTable = ToolboxService::getTableByModel($strAttributeValueClass);

        // Get the Item Class
        $strItemClass = preg_replace('/AttributeModel/', 'Model', $strAttributeClass);

        // Get Table from Item Class
        $strTable = ToolboxService::getTableByModel($strItemClass);

        if(!$strTable){
            throw new \Exception('Could not find Table for Model ' . $strItemClass . '. Please make sure the Model is named correctly.');
        }

        // Get the current language
        $strLanguage = $GLOBALS['TL_LANGUAGE'];

        // Get the default language
        $strDefaultLanguage = System::getContainer()->get('memo.foundation.language')->getDefaultLanguage();

        // Define default return value
        $arrOptions = array();

        // Should a blank option be added?
        if ($bolAddBlankOption) {
            $strTitle = ($this->title_multiple) ?: $this->title;
            $arrOptions['*'] = $strTitle;
        }

        // Is a static table defined?
        if ($this->foreignKey) {
            $strField = $this->foreignKeyField ?: 'title';

            if ($arrValues = Database::getInstance()->prepare("SELECT * FROM " . $this->foreignKey . " WHERE 1 ORDER BY " . $strField . " ASC")->execute()->fetchAllAssoc()) {

                foreach ($arrValues as $arrValue) {

                    $bolPublished = true;
                    if (array_key_exists('start', $arrValue) && $arrValue['start'] != '' && $arrValue['start'] > time()) {
                        $bolPublished = false;
                    }
                    if (array_key_exists('stop', $arrValue) && $arrValue['stop'] != '' && $arrValue['stop'] < time()) {
                        $bolPublished = false;
                    }
                    if (array_key_exists('published', $arrValue) && $arrValue['published'] != 1) {
                        $bolPublished = false;
                    }
                    if (array_key_exists('invisible', $arrValue) && $arrValue['published'] == 1) {
                        $bolPublished = false;
                    }

                    if ($bolPublished) {
                        $arrOptions[$arrValue['id']] = $arrValue[$strField];
                    }
                }
            }

        } else {

            // Default case: get the attribute values
            $arrColumns = array("$strAttributeValueTable.pid=?", "$strAttributeValueTable.published=?", "($strAttributeValueTable.start='' OR $strAttributeValueTable.start<?)", "($strAttributeValueTable.stop='' OR $strAttributeValueTable.stop>?)");
            $arrValues = array($this->id, 1, time(), time());

            if ($colOptions = $strAttributeValueClass::findBy($arrColumns, $arrValues, array('order' => 'sorting ASC'))) {
                foreach ($colOptions as $objOption) {

                    // Are translations needed?
                    if ($strLanguage != $strDefaultLanguage) {
                        $objOption->getTranslatedModel($strLanguage);
                    }

                    // Add option to return array
                    if($bolValues){
                        if($objOption->value != ''){
                            $arrOptions[$objOption->id] = $objOption->value;
                        } elseif($objOption->alias != '') {
                            $arrOptions[$objOption->id] = $objOption->alias;
                        } else {
                            $arrOptions[$objOption->id] = $objOption->title;
                        }

                    } else {
                        $arrOptions[$objOption->id] = $objOption->title;
                    }

                }
            }
        }

        if ($bolOnlyActive) {

            if (!$colItems) {
                $colItems = $strItemClass::findAllPublished();
            }

            $arrActiveOptions = array();

            if ($bolAddBlankOption) {
                $strTitle = ($this->title_multiple) ?: $this->title;
                $arrActiveOptions[''] = $strTitle;
            }

            foreach ($colItems as $objProduct) {
                $strFieldname = $this->getFieldName();
                $strValue = $objProduct->$strFieldname;
                if ($this->multiple) {
                    if ($strValue) {
                        $arrValues = unserialize($strValue, array(true));
                        if (is_array($arrValues)) {
                            foreach ($arrValues as $strValue) {
                                if (array_key_exists($strValue, $arrOptions)) {
                                    $arrActiveOptions[$strValue] = $arrOptions[$strValue];
                                }
                            }
                        }
                    }
                } else {
                    if (array_key_exists($strValue, $arrOptions)) {
                        $strLabel = $arrOptions[$strValue];
                        $arrActiveOptions[$strValue] = $strLabel;
                    }
                }
            }

            $arrOptions = $arrActiveOptions;

        }
        return $arrOptions;
    }

    public function getFieldName()
    {
        return 'attr_' . $this->alias;
    }

    public function getLowestValue($colProducts = false)
    {
        $arrData = array();

        // Get the Attribute Model Class
        $strAttributeClass = get_class($this);

        // Get the Item Class
        $strItemClass = preg_replace('/AttributeModel/', 'Model', $strAttributeClass);

        if (!$colProducts) {
            $colProducts = $strItemClass::findAllPublished();
        }

        $strFieldname = $this->getFieldName();

        if ($colProducts) {
            foreach ($colProducts as $objProduct) {

                if ($objProduct->$strFieldname == '' || $objProduct->$strFieldname == null) {
                    continue;
                }

                if ($this->filter_type == 'range') {
                    $strFieldnameTo = str_replace('_from', '_to', $strFieldname);
                    $arrData[] = $objProduct->$strFieldname;
                    $arrData[] = $objProduct->$strFieldnameTo;
                } else {
                    $arrData[] = $objProduct->$strFieldname;
                }
            }

            return min($arrData);
        }

        return 1;
    }

    public function getHighestValue($colProducts = false)
    {
        $arrData = array();

        // Get the Attribute Model Class
        $strAttributeClass = get_class($this);

        // Get the Item Class
        $strItemClass = preg_replace('/AttributeModel/', 'Model', $strAttributeClass);

        if (!$colProducts) {
            $colProducts = $strItemClass::findAllPublished();
        }

        $strFieldname = $this->getFieldName();

        if ($colProducts) {
            foreach ($colProducts as $objProduct) {

                if ($objProduct->$strFieldname == '' || $objProduct->$strFieldname == null) {
                    continue;
                }

                if ($this->filter_type == 'range') {
                    $strFieldnameTo = str_replace('_from', '_to', $strFieldname);
                    $arrData[] = $objProduct->$strFieldname;
                    $arrData[] = $objProduct->$strFieldnameTo;
                } else {
                    $arrData[] = $objProduct->$strFieldname;
                }
            }

            return max($arrData);
        }

        return 100;
    }

    public function generateDca($bolForeignKey = true)
    {

        // Get the Attribute Model Class
        $strAttributeClass = get_class($this);

        // Generate the attribute value class name
        $strAttributeValueClass = preg_replace('/Model$/', 'ValueModel', $strAttributeClass);

        // Get the Item Class
        $strItemClass = preg_replace('/AttributeModel/', 'Model', $strAttributeClass);

        // Get the ItemName
        preg_match('/Model\\\(.*?)Attribute/', $strAttributeClass, $arrMatches);
        $strItemName = false;
        if(is_array($arrMatches) && count($arrMatches) > 0) {
            $strItemName = $arrMatches[1];
        }

        if(!$strItemName){
            throw new \Exception('Could not find ItemName in AttributeModel, for example Product in Memo\ProductBundle\Model\ProductAttributeModel. Please make sure the Model is named correctly.');
        }

        // Generate Hook name
        $strHookName = 'generate' . $strItemName . 'AttributeDynamicDCA';

        $arrField = array(
            'label' => array($this->title, (!is_null($this->explanation)) ? $this->explanation : ''),
            'exclude' => true,
            'filter' => false,
            'translate' => $this->translate,
            'search' => false,
            'sorting' => false,
            'inputType' => $this->type,
            'eval' => array('tl_class' => 'w50', 'mandatory' => $this->mandatory, 'datepicker' => $this->datepicker),
            'sql' => "varchar(255) NOT NULL default '" . $this->default . "'",
        );

        if ($this->datepicker) {
            $arrField['eval']['tl_class'] .= ' wizard';
        }

        if ($this->default != '') {
            $arrField['default'] = $this->default;
        }

        if ($this->rgxp != '') {
            $arrField['eval']['rgxp'] = $this->rgxp;
        }

        switch ($this->type) {
            case 'text':
                $arrField['search'] = true;
                break;
            case 'textarea':
                if ($this->tinymce) {
                    $arrField['eval']['rte'] = 'tinyMCE';
                }
                $arrField['eval']['tl_class'] = 'clr long';
                $arrField['sql'] = "text NULL";
                break;
            case 'select':
                $arrField['filter'] = true;
                if ($this->foreignKey == '') {
                    $colValues = $strAttributeValueClass::findBy(array("pid=?"), array($this->id), array('order' => 'sorting ASC'));
                    $arrField['options'] = ToolboxService::collectionToArray($colValues);
                    if ($bolForeignKey) {
                        $arrField['foreignKey'] = 'tl_memo_product_attribute_value.title';
                    }
                    $arrField['relation']['table'] = 'tl_memo_product_attribute_value';
                    $arrField['relation']['type'] = 'hasOne';
                    $arrField['relation']['load'] = 'eager';
                } else {
                    $arrField['foreignKey'] = $this->foreignKey . ($this->foreignKeyField ? '.' . $this->foreignKeyField : '.title');
                    $arrField['relation']['table'] = $this->foreignKey;
                    $arrField['relation']['type'] = 'hasOne';
                    $arrField['relation']['load'] = 'lazy';
                }

                if ($this->mandatory) {
                    $arrField['eval']['includeBlankOption'] = false;
                } else {
                    $arrField['eval']['includeBlankOption'] = true;
                }
                $arrField['eval']['multiple'] = $this->multiple;
                if ($this->multiple) {
                    $arrField['sql'] = "blob null";
                    $arrField['relation'] = array(
                        'type' => 'haste-ManyToMany',
                        'load' => 'lazy',
                        'table' => 'tl_memo_product_attribute_value',
                        'relationTable' => 'tl_memo_product_' . $this->alias,
                        'forceSave' => true,
                    );
                } else {
                    $arrField['sql'] = "int(10) unsigned NULL";
                }
                $arrField['eval']['chosen'] = true;
                break;
            case 'checkbox':
                $arrField['filter'] = true;
                $arrField['eval']['tl_class'] = 'w50 m12';
                break;
            case 'link':
                $arrField['inputType'] = 'text';
                $arrField['eval']['rgxp'] = 'url';
                $arrField['eval']['decodeEntities'] = true;
                $arrField['eval']['dcaPicker'] = true;
                $arrField['eval']['tl_class'] = 'w50 wizard';
                break;
            case 'image':
                $arrField['inputType'] = 'fileTree';
                $arrField['eval']['files'] = true;
                $arrField['eval']['filesOnly'] = true;
                $arrField['eval']['extensions'] = Config::get('validImageTypes');
                $arrField['eval']['multiple'] = $this->multiple;
                if ($this->multiple) {
                    $arrField['eval']['fieldType'] = 'checkbox';
                    $arrField['sql'] = "blob null";
                    $arrField['eval']['orderField'] = 'orderSRC' . $this->id;

                    $GLOBALS['TL_DCA']['tl_memo_product']['fields']['orderSRC' . $this->id] = array(
                        'sql' => "blob NULL"
                    );

                } else {
                    $arrField['eval']['fieldType'] = 'radio';
                    $arrField['sql'] = "binary(16) NULL";
                }
                $arrField['eval']['tl_class'] = 'clr long';
                break;
        }

        if ($this->tl_class) {
            $arrField['eval']['tl_class'] = $this->tl_class;
        }

        // Enable a hook to be loaded here, example for product: generateProductAttributeDynamicDCA or for jobs generateJobAttributeDynamicDCA
        if (isset($GLOBALS['TL_HOOKS'][$strHookName]) && is_array($GLOBALS['TL_HOOKS'][$strHookName])) {
            foreach ($GLOBALS['TL_HOOKS'][$strHookName] as $callback) {
                $arrField = System::importStatic($callback[0])->{$callback[1]}($arrField, $this);
            }
        }

        return $arrField;
    }
}
