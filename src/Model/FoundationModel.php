<?php

declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\FoundationBundle\Model;

use Contao\Controller;
use Contao\Database;
use Contao\PageModel;
use Contao\System;
use Contao\ModuleModel;
use Codefog\HasteBundle\Model\DcaRelationsModel;
use Memo\FoundationBundle\Service\ToolboxService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Terminal42\ChangeLanguage\PageFinder;

/**
 * Class FoundationModel
 *
 * Enable ease Translation of all fields in DCA
 */
class FoundationModel extends DcaRelationsModel
{

    /**
     * Returns a item, based on the alias, regardless of the language
     * @param $strAlias
     * @param $bolIgnorePublished (only show published by default)
     * @return $objItem
     */
    public static function getItemByTranslatedAlias($strAlias, $bolIgnorePublished = false, $strCurrentLanguage = false, $strField = "alias")
    {
        // Presets
        $strTable = static::$strTable;
        Controller::loadDataContainer($strTable);
        $strCurrentLanguage = ($strCurrentLanguage) ? $strCurrentLanguage : $GLOBALS['TL_LANGUAGE'];


        // Get Default Language
        $strDefaultLanguage = System::getContainer()->get('memo.foundation.language')->getDefaultLanguage();

        // Get Items
        if ($strCurrentLanguage != $strDefaultLanguage && $GLOBALS['TL_DCA'][$strTable]['fields'][$strField]['translate'] === true) {
            $strFieldname = $strField . '_' . $strCurrentLanguage;

            if ($bolIgnorePublished) {

                $colItem = static::findBy(
                    array("$strTable.$strFieldname=?"),
                    array($strAlias)
                );

            } else {

                $colItem = static::findBy(
                    array("$strTable.$strFieldname=?", "$strTable.published=?", "($strTable.start='' OR $strTable.start<?)", "($strTable.stop='' OR $strTable.stop>?)"),
                    array($strAlias, 1, time(), time())
                );

            }

            if (!$colItem) {
                if ($bolIgnorePublished) {

                    $colItem = static::findBy(
                        array("$strTable.$strField=?"),
                        array($strAlias)
                    );

                } else {

                    $colItem = static::findBy(
                        array("$strTable.$strField=?", "$strTable.published=?", "($strTable.start='' OR $strTable.start<?)", "($strTable.stop='' OR $strTable.stop>?)"),
                        array($strAlias, 1, time(), time())
                    );

                }

            }

        } else {

            if ($bolIgnorePublished) {

                $colItem = static::findBy(
                    array("$strTable.$strField=?"),
                    array($strAlias)
                );

            } else {

                $colItem = static::findBy(
                    array("$strTable.$strField=?", "$strTable.published=?", "($strTable.start='' OR $strTable.start<?)", "($strTable.stop='' OR $strTable.stop>?)"),
                    array($strAlias, 1, time(), time())
                );

            }
        }

        // Reset Data that was modified (for Default-Language)
        if ($colItem) {
            foreach ($colItem as $objItem) {
                if (is_array($objItem->arrModified) && count($objItem->arrModified) > 0) {
                    foreach ($objItem->arrModified as $keyField => $strValue) {
                        $objItem->$keyField = $strValue;
                    }
                }
            }
        }


        return $colItem;
    }

    /**
     * Returns an array of all Page-IDs, on which auto_item Elements could be used on
     * @param bool $bolTranslatedPages
     * @return array
     */
    public static function getDetailPages($bolTranslatedPages = false, $bolOnlyActiveLanguages = false)
    {
        // Presets
        $objLanguageService = System::getContainer()->get('memo.foundation.language');
        $arrLanguages = $objLanguageService->getAllLanguages(true, $bolOnlyActiveLanguages);
        $arrPages = array();

        // Only continue, if the archive-table exists
        $objDatabase = Database::getInstance();
        if ($objDatabase->tableExists(static::getTable()) === false) {
            return $arrPages;
        }

        // Get all archives
        $objArchives = static::findAll();

        // Look for jumpTo Pages in the Archives
        if ($objArchives) {
            foreach ($objArchives as $objArchive) {
                if ($objArchive->jumpTo) {
                    $arrPages[$objArchive->jumpTo] = $objArchive->jumpTo;
                }
                if ($bolTranslatedPages) {
                    foreach ($arrLanguages as $strLanguage) {
                        $strFieldname = "jumpTo" . $objLanguageService->getLanguagePostfix($strLanguage);
                        if ($objArchive->$strFieldname) {
                            $arrPages[$objArchive->$strFieldname] = $objArchive->$strFieldname;
                        }
                    }
                } else {
                    if ($objArchive->jumpTo) {
                        $arrPages[$objArchive->jumpTo] = $objArchive->jumpTo;
                    }
                }
            }
        }

        // Get all modules, that overwrite the jumpto from the archive and get the page ids
        $objDb = \Contao\Database::getInstance();
        $objResult = $objDb->prepare('SELECT jumpTo FROM tl_module WHERE tl_module.jumpTo != ? AND tl_module.foundation_item_template != ?')->execute('', '');
        if ($objResult && $objResult->numRows > 0) {
            while ($objResult->next()) {
                $arrPages[$objResult->jumpTo] = $objResult->jumpTo;
            }
        }

        // Get the pages in all languages
        if ($bolTranslatedPages && is_array($arrPages)) {

            $arrOrigPages = $arrPages;
            $objPageFinder = new PageFinder();
            foreach ($arrLanguages as $strLanguage) {
                foreach ($arrOrigPages as $intPage) {
                    try {

                        $objTranslatedPage = $objPageFinder->findAssociatedForLanguage(PageModel::findByPk($intPage), $strLanguage);

                    } catch (\InvalidArgumentException $e) {
                        // Do nothing - this can happen, if there are multiple domains in the same installation
                    }

                    if ($objTranslatedPage) {
                        $arrPages[$objTranslatedPage->id] = $objTranslatedPage->id;
                    }


                }
            }

        }

        return $arrPages;
    }

    /**
     * Returns the object, with all translated (or default) values
     * @param $strLanguage
     * @return $this
     */
    public function getTranslatedModel($strLanguage)
    {
        // Get Language Postfix
        $strPostfix = System::getContainer()->get('memo.foundation.language')->getLanguagePostfix($strLanguage);

        // Only run, if there is a Postfix
        if ($strPostfix != '') {
            // Loop all dca-fields
            foreach ($this->arrData as $key => $value) {
                // Define the field-name for the wanted translation
                $strLanguageField = $key . $strPostfix;

                // Check if the field exists and if there is a value set (to override the default-value)
                if (
                    array_key_exists($strLanguageField, $this->arrData) &&
                    $this->$strLanguageField != '' &&
                    $this->$strLanguageField != null) {
                    // Set new value (only for this call)
                    $this->$key = $this->$strLanguageField;
                }
            }
        }

        // Return the translated object
        return $this;
    }

    /**
     * @param $arrPids
     * @param array $arrOptions
     * @return |null
     */
    public static function findPublishedByPids($arrPids, array $arrOptions = array(), array $arrColumns = array(), $arrValues = array())
    {

        // Return null, if no data found
        if (empty($arrPids) || !\is_array($arrPids)) {
            return null;
        }

        // Presets
        $strTable = static::$strTable;

        // Set Filter for Archives
        $arrColumns[] = "$strTable.pid IN(" . implode(',', array_map('\intval', $arrPids)) . ")";

        // Set Filter for Published
        $arrColumns[] = "$strTable.published='1'";

        // Set Filter for stop/start
        $arrColumns[] = "($strTable.start='' OR $strTable.start<" . time() . ")";
        $arrColumns[] = "($strTable.stop='' OR $strTable.stop>" . time() . ")";

        // Get the Items
        $colItems = static::findBy($arrColumns, $arrValues, $arrOptions);

        // Return the Items
        return $colItems;
    }

    /**
     * @param $arrIds
     * @param array $arrOptions
     * @param array $arrColumns
     * @return \Contao\Model|\Contao\Model[]|\Contao\Model\Collection|FoundationModel|null
     */
    public static function findPublishedByIds($arrIds, array $arrOptions = array(), array $arrColumns = array())
    {
        // Return null, if no data found
        if (empty($arrIds) || !\is_array($arrIds)) {
            return null;
        }

        // Presets
        $strTable = static::$strTable;

        // Set Filter for Archives
        $arrColumns[] = "$strTable.id IN(" . implode(',', array_map('\intval', $arrIds)) . ")";

        // Set Filter for Published
        $arrColumns[] = "$strTable.published='1'";

        // Set Filter for stop/start
        $arrColumns[] = "($strTable.start='' OR $strTable.start<" . time() . ")";
        $arrColumns[] = "($strTable.stop='' OR $strTable.stop>" . time() . ")";

        // Get the Items
        $colItems = static::findBy($arrColumns, null, $arrOptions);

        // Return the Items
        return $colItems;
    }

    public static function findAllPublished(array $arrOptions = array())
    {

        if (empty($arrOptions)) {
            $arrOptions = array('order' => 'title');
        }

        // Presets
        $strTable = static::$strTable;

        // Set Filter for Published
        $arrColumns[] = "$strTable.published='1'";

        // Set Filter for stop/start
        $arrColumns[] = "($strTable.start='' OR $strTable.start<" . time() . ")";
        $arrColumns[] = "($strTable.stop='' OR $strTable.stop>" . time() . ")";

        // Get the Items
        $colItems = static::findBy($arrColumns, null, $arrOptions);

        // Return the Items
        return $colItems;
    }

    public function getModified()
    {
        return $this->arrModified;
    }

    public function getURL($strLanguage = false, $strType = UrlGeneratorInterface::ABSOLUTE_URL, $bolForSitemap = false)
    {

        // Get the language
        if (!$strLanguage) {
            $strLanguage = $GLOBALS['TL_LANGUAGE'];
        }

        // Get the default language
        $strDefaultLanguage = System::getContainer()->get('memo.foundation.language')->getDefaultLanguage();

        // Get Tag in correct language
        if ($strLanguage != $strDefaultLanguage) {
            $this->getTranslatedModel($strLanguage);
        }

        // Static URL set?
        if ($this->url != '') {
            return $this->url;
        }

        // Get the archive and translate it
        $objArchive = $this->getRelated('pid');
        if ($strLanguage != $strDefaultLanguage) {
            $objArchive->getTranslatedModel($strLanguage);
        }

        if (!$objArchive) {
            return false;
        }

        if ($bolForSitemap && stristr($objArchive->robots, 'noindex')) {
            return false;
        }

        if (!$objArchive->jumpTo) {
            return false;
        }

        if ($bolForSitemap && $objArchive->published != 1) {
            return false;
        }

        // Get the page
        $objDetailPage = $objArchive->getRelated('jumpTo');

        if ($strLanguage != $strDefaultLanguage) {
            $objPageFinder = new PageFinder();

            // Check if the page exists
            if ($objDetailPage && $objPageFinder) {

                // Get the page in the correct language
                $objDetailPage = $objPageFinder->findAssociatedForLanguage($objDetailPage, $strLanguage);

            }
        }

        if (!$objDetailPage) {
            return false;
        }

        if ($bolForSitemap && stristr($objDetailPage->robots, 'noindex')) {
            return false;
        }

        if (!$this->alias) {

            if($bolUseListingAsFallback && $objArchive && $objArchive->jumpToListing){

                if($objListingPage = $objArchive->getRelated('jumpToListing')){

                    if($strLanguage != $strDefaultLanguage){
                        $objPageFinder = new PageFinder();
                        $objListingPage = $objPageFinder->findAssociatedForLanguage($objListingPage, $strLanguage);
                    }

                    if($objListingPage){
                        return $objListingPage->getAbsoluteUrl();
                    }
                }

            }

            return false;
        }
        if (method_exists($objDetailPage, 'getAbsoluteUrl')) {

            // Generate absolute url
            $strLink = $objDetailPage->getAbsoluteUrl('/' . $this->alias);

        } else {

            // Get the url generator
            $urlGenerator = System::getContainer()->get('contao.routing.url_generator');

            // Defined the parameters array
            $arrParams = array(
                '_locale' => $strLanguage,
                'alias' => $this->alias,
                'auto_item' => 'alias'
            );

            $strLink = $urlGenerator->generate($objDetailPage->alias . '/{alias}', $arrParams, $strType);

        }

        return $strLink;
    }

    public static function countByFormData($strLanguage = 'de', $intOffset = 0, $intLimit = 0, $arrFormData = array())
    {
        // Get Tables
        $strTable = static::$strTable;

        // Define the default filter values
        $arrColumns = array("$strTable.published=?", "($strTable.start='' OR $strTable.start<?)", "($strTable.stop='' OR $strTable.stop>?)");
        $arrValues = array(1, time(), time());

        // Add defaut count
        $intProducts = 0;

        // Add the form filters
        if (class_exists('\Memo\ProductBundle\Model\ProductAttributeModel')) {

            $arrFormFilters = \Memo\ProductBundle\Model\ProductAttributeModel::resolveFilterForm($arrFormData);
            $arrColumns = array_merge($arrColumns, $arrFormFilters[0]);
            $arrValues = array_merge($arrValues, $arrFormFilters[1]);

            // Get the product count
            $intProducts = self::countBy($arrColumns, $arrValues);

        }

        // Return the product count
        return $intProducts;
    }

    public static function findByModule($objModule, $bolOnlyPublished=true, $arrOptions = array(), $bolTranslated = false)
    {

        // Get the language
        $strLanguage = $GLOBALS['TL_LANGUAGE'];

        // Get the Module or Element data
        $arrArchives = false;
        if($objModule->foundation_archives){
            $arrArchives = unserialize($objModule->foundation_archives);
        }
        $arrSelection = false;
        if($objModule->foundation_item_selection){
            $arrSelection = unserialize($objModule->foundation_item_selection);
        }
        $strFeatured = $objModule->foundation_featured;
        $strSQLFilter = $objModule->sql_filter;
        $arrCategories = false;

        if($objModule->categories_filter){
            $arrCategories = unserialize($objModule->categories_filter);
        }
        if($objModule->categories_filter_type){
            $arrCategoryFilterType = $objModule->categories_filter_type;
        }
        $strOrder = $objModule->foundation_order;
        $strOrderField = $objModule->foundation_order_field;

        // Get the default language
        $strDefaultLanguage = System::getContainer()->get('memo.foundation.language')->getDefaultLanguage();

        // Get Tables
        $strTable = static::$strTable;

        // Default - no filtering
        $arrColumns = array();
        $arrValues = array();

        // Generate the options
        if(!isset($arrOptions['order'])){
            $arrOptions['order'] = ToolboxService::convertOrderString($strOrder, $strTable, $strOrderField);
        }

        // Filter by Published and start/stop
        if($bolOnlyPublished){

            // Define the default filter values
            $arrColumns = array("$strTable.published=?", "($strTable.start='' OR $strTable.start<?)", "($strTable.stop='' OR $strTable.stop>?)");
            $arrValues = array(1, time(), time());

        }

        // Filter by Archive
        if($arrArchives) {

            $arrColumns[] = implode(' OR ', array_fill(0, count($arrArchives), "$strTable.pid=?"));

            foreach($arrArchives as $intArchive){

                $arrValues[] = $intArchive;
            }

        }

        // Filter by Category
        if($arrCategories && !stristr($objModule->type, 'custom')){

            if ($arrCategoryFilterType == 'and') {
                foreach ($arrCategories as $intCategoryFilter) {
                    $arrColumns[] = "$strTable.categories LIKE ?";
                    $arrValues[] = '%"' . $intCategoryFilter . '"%';
                }
            } else {
                $strColumns = '(';
                foreach ($arrCategories as $intCategoryFilter) {
                    $strColumns .= "$strTable.categories LIKE ? OR ";
                    $arrValues[] = '%"' . $intCategoryFilter . '"%';
                }
                $strColumns = substr($strColumns, 0, -3);
                $strColumns .= ')';
                $arrColumns[] = $strColumns;
            }

        }

        // Filter by Featured
        if($strFeatured && !stristr($objModule->type, 'custom')) {
            switch($strFeatured){
                case 'only_featured':
                    $arrColumns[] = "$strTable.featured=1";
                    break;
                case 'only_not_featured':
                    $arrColumns[] = "$strTable.featured!=1";
                    break;
                case 'all':
                default:
                    break;
            }
        }

        // Filter by Selection
        if($arrSelection && stristr($objModule->type, 'custom')) {
            $arrColumns[] = "$strTable.id IN(" . implode(',', array_map('\intval', $arrSelection)) . ")";
            $arrOptions['order'] = "FIELD($strTable.id," . implode(',', array_map('\intval', $arrSelection)) . ")";
        }

        // Filter by raw sql statement (fallback)
        if($strSQLFilter && !stristr($objModule->type, 'custom')) {
            $arrColumns[] = $strSQLFilter;
        }

        // Get the items
        $colItems = self::findBy($arrColumns, $arrValues, $arrOptions);

        if(!$bolTranslated){
            return $colItems;
        }

        $arrItems = array();

        if (!$colItems) {
            return $arrItems;
        }

        foreach ($colItems as $objItem) {
            if ($strLanguage != $strDefaultLanguage) {
                $objItem->getTranslatedModel($strLanguage);
            }

            $arrItems[] = $objItem;
        }

        // Return the items
        return $arrItems;
    }

    public static function findByFormData($strLanguage = false, $intOffset = 0, $intLimit = 0, $arrFormData = array(), $arrOptions = array())
    {

        // Get the language
        if (!$strLanguage) {
            $strLanguage = $GLOBALS['TL_LANGUAGE'];
        }

        // Get the default language
        $strDefaultLanguage = System::getContainer()->get('memo.foundation.language')->getDefaultLanguage();

        // Get Tables
        $strTable = static::$strTable;

        // Define the default filter values
        $arrColumns = array("$strTable.published=?", "($strTable.start='' OR $strTable.start<?)", "($strTable.stop='' OR $strTable.stop>?)");
        $arrValues = array(1, time(), time());

        // Add the form filters
        if (class_exists('\Memo\ProductBundle\Model\ProductAttributeModel')) {
            $arrFormFilters = \Memo\ProductBundle\Model\ProductAttributeModel::resolveFilterForm($arrFormData);
            $arrColumns = array_merge($arrColumns, $arrFormFilters[0]);
            $arrValues = array_merge($arrValues, $arrFormFilters[1]);
        }

        // Add the offset and limit
        $arrOptions = array_merge(array('offset' => $intOffset, 'limit' => $intLimit), $arrOptions);

        // Get the items
        $colItems = self::findBy($arrColumns, $arrValues, $arrOptions);

        $arrItems = array();

        if (!$colItems) {
            return $arrItems;
        }

        foreach ($colItems as $objItem) {
            if ($strLanguage != $strDefaultLanguage) {
                $objItem->getTranslatedModel($strLanguage);
            }

            $arrItems[] = $objItem;
        }

        // Return the items
        return $arrItems;
    }

    public function getAttributeValue(string $strAttributeAlias, bool $bolReturnObject = false, string $strForeignKey = '')
    {
        // Get item class
        $strItemClass = get_class($this);

        // Generate the attribute class name
        $strAttributeClass = preg_replace('/Model$/', 'AttributeModel', $strItemClass);

        // Generate the attribute value class name
        $strAttributeValueClass = preg_replace('/Model$/', 'AttributeValueModel', $strItemClass);

        // Get the Attribute
        $objAttribute = $strAttributeClass::findOneBy('alias', $strAttributeAlias);

        if(!$objAttribute){

            if(property_exists($this, $strAttributeAlias)){
                return $this->$strAttributeAlias;
            }

            return '';
        }

        // Get the fieldname
        $strFieldname = $objAttribute->getFieldName();

        if($strFieldname == ''){
            return '';
        }

        // Get the value
        $strValue = $this->$strFieldname;

        if($strValue == ''){
            return '';
        }

        // Based on the type of the attribute, we need to format the value
        switch ($objAttribute->type) {
            case 'list':
                if ($arrValues = unserialize($strValue)) {
                    $strValue = "<ul><li>" . implode('</li><li>', $arrValues) . "</li></ul>";
                }
                break;
            case 'select':
                $arrOptions = $objAttribute->getOptions(false, false, true, true);

                if($objAttribute->multiple){

                    $arrValues = unserialize($strValue);
                    $arrNewValues = array();
                    foreach($arrValues as $strValue){
                        if($bolReturnObject) {
                            $arrNewValues[] = $strAttributeValueClass::findByPk($strValue);
                        } else {
                            if (array_key_exists($strValue, $arrOptions)) {
                                $arrNewValues[] = $arrOptions[$strValue];
                            }
                        }
                    }
                    $strValue = $arrNewValues;
                } else {
                    if($arrOptions && array_key_exists($strValue, $arrOptions)){

                        if($bolReturnObject){
                            $strValue = $strAttributeValueClass::findByPk($strValue);
                        } else {
                            $strValue = $arrOptions[$strValue];
                        }
                    }
                }

                if(isset($objAttributeValue)){
                    return $objAttributeValue->title;
                }

                break;
            default:
                // Do nothing and use the saved value
                break;
        }

        return $strValue;
    }
}
