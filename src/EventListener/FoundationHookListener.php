<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\FoundationBundle\EventListener;

use Contao\PageModel;
use Contao\System;
use Contao\Backend;
use Contao\Controller;

class FoundationHookListener extends Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function generateSitemapOrSearchIndex($arrPages, $strItemTable, $blnSitemap = false, $strLanguage = null, $intRoot = null, $bolStartStop = false): array
    {
        // Detect if its for the sitemap (in that case we get a language)
        if ($blnSitemap && $strLanguage) {

            unset($arrPagesTemp);

            $arrPagesTemp = self::generateArchiveItemLinks($strLanguage, $strItemTable, $intRoot, $bolStartStop);
            if (is_array($arrPagesTemp)) {
                $arrPages = array_merge($arrPages, $arrPagesTemp);
            }

            // Or if its for the search (in that case get loop through all languages)
        } else {

            // Get all languages
            $arrLanguages = System::getContainer()->get('memo.foundation.language')->getAllLanguages(true, true);

            if (is_array($arrLanguages)) {

                // Loop all languages
                foreach ($arrLanguages as $strLanguage) {

                    unset($arrPagesTemp);

                    $arrPagesTemp = self::generateArchiveItemLinks($strLanguage, $strItemTable, null, true);

                    if (is_array($arrPagesTemp)) {
                        $arrPages = array_merge($arrPages, $arrPagesTemp);
                    }

                }
            }
        }

        return $arrPages;
    }

    public static function generateArchiveItemLinks($strLanguage, $strItemTable, $intRoot = null, $bolStartStop = false): array
    {
        ;
        unset($arrPagesTemp);
        $arrPagesTemp = array();
        $strDefaultLanguage = System::getContainer()->get('memo.foundation.language')->getDefaultLanguage();

        // Get the Item-Model and DCA
        $strItemModel = $GLOBALS['TL_MODELS'][$strItemTable];
        Controller::loadDataContainer($strItemTable);

        // Get all the Items to this archive
        if ($bolStartStop) {
            $colItems = $strItemModel::findBy(
                array("$strItemTable.published=?", "($strItemTable.start='' OR $strItemTable.start<?)", "($strItemTable.stop='' OR $strItemTable.stop>?)"),
                array(1, time(), time())
            );

        } else {
            $colItems = $strItemModel::findBy(
                array("$strItemTable.published=?"),
                array(1)
            );
        }

        if ($colItems) {

            $contaoFramework = System::getContainer()->get('contao.framework');
            $contaoFramework->initialize();

            // Loop the Items
            foreach ($colItems as $objItem) {
                // Translate the Item
                if ($strLanguage != $strDefaultLanguage) {
                    $objItem->getTranslatedModel($strLanguage);
                }

                // Is there an alias?
                if (!$objItem->alias) {
                    continue;
                }

                // Generate the URL
                $strURL = $objItem->getURL($strLanguage);

                // Enable a hook to be loaded here
                if (isset($GLOBALS['TL_HOOKS']['addItemToSitemap']) && \is_array($GLOBALS['TL_HOOKS']['addItemToSitemap'])) {
                    foreach ($GLOBALS['TL_HOOKS']['addItemToSitemap'] as $callback) {
                        $strURL = array(...$strURL, ...System::importStatic($callback[0])->{$callback[1]}($strURL, $objItem, $arrArchiveDetailpages[$objItem->pid], $strItemTable, false));
                    }
                }

                if ($strURL != false) {

                    // Add the URL to our array of urls
                    $arrPagesTemp[] = $strURL;

                }

            }
        }

        // Return the Item-URLs
        return $arrPagesTemp;
    }
}
