<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\FoundationBundle\EventListener;

use Contao\CoreBundle\Event\ContaoCoreEvents;
use Contao\CoreBundle\Event\PreviewUrlCreateEvent;
use Contao\Input;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Terminal42\ServiceAnnotationBundle\Annotation\ServiceTag;

/**
 * @ServiceTag("kernel.event_listener", event=ContaoCoreEvents::PREVIEW_URL_CREATE)
 */
class PreviewUrlCreateListener
{
    public function __invoke(PreviewUrlCreateEvent $event): void
    {

        // Get general data
        $strTable = Input::get('table');
        $intID = Input::get('id');

        // Only manage tables and IDs
        if (!$strTable || !$intID) {
            return;
        }

        // Only manage memo tables
        if (!stristr($strTable, 'tl_memo')) {
            return;
        }

        // Only manage child records, not the parent
        if (stristr($strTable, 'archive')) {
            return;
        }

        // Check Model
        if (!array_key_exists($strTable, $GLOBALS['TL_MODELS'])) {
            return;
        }

        // Get Model
        $strModel = $GLOBALS['TL_MODELS'][$strTable];

        // Check class
        if (!class_exists($strModel)) {
            return;
        }

        // Get Item
        $objItem = $strModel::findByPk($intID);

        // Check Item
        if (!$objItem) {
            return;
        }

        // Check if url method exists
        if (!method_exists($objItem, 'getURL')) {
            return;
        }

        // Get URL
        $strURL = $objItem->getURL(false, UrlGeneratorInterface::RELATIVE_PATH);

        // Check URL
        if (!$strURL) {
            return;
        }

        // Set new query URL
        $event->setQuery('url=' . $strURL);
    }
}
