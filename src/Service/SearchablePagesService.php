<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\FoundationBundle\Service;

use Contao\PageModel;

class SearchablePagesService
{
    /**
     * Returns the array of child-urls for a given table
     * @param    $strTable        Tablename, in which the child-records are
     * @return $arrPages        Array of child-urls for the sitemap
     **/
    public function getUrls($strTable, $intRoot = null, $blnSitemap = false, $strLanguage = null)
    {
        // Get the Root-Page
        $objRootPage = PageModel::findByPk($intRoot);

        // Detect if the request is for the sitemap (in that case we get a language) instead of the search-indexer
        if ($blnSitemap) {

            $arrPagesTemp = self::getProjectsByLanguage($strLanguage, $objRootPage);

            if (is_array($arrPagesTemp)) {
                $arrPages = array_merge($arrPages, $arrPagesTemp);
            }

            // Or if its for the search (in that case get loop through all languages)
        } else {

            // Get all languages
            $objDatabase = Database::getInstance();
            $objResult = $objDatabase->prepare("SELECT * FROM tl_page WHERE type='root'")->execute();
            $arrLanguages = $objResult->fetchAllAssoc();

            if (is_array($arrLanguages)) {

                // Loop all languages
                foreach ($arrLanguages as $arrLanguage) {

                    $objRootPage = PageModel::findByPk($arrLanguage['id']);

                    $arrPagesTemp = self::getProjectsByLanguage($arrLanguage['language'], $objRootPage);

                    if (is_array($arrPagesTemp)) {
                        $arrPages = array_merge($arrPages, $arrPagesTemp);
                    }

                }
            }
        }

        return $arrPages;
    }
}
