<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\FoundationBundle\Service;

use Contao\Config;
use Contao\Controller;
use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\CoreBundle\Routing\ResponseContext\HtmlHeadBag\HtmlHeadBag;
use Contao\CoreBundle\Routing\ResponseContext\JsonLd\JsonLdManager;
use Contao\DataContainer;
use Contao\Dbafs;
use Contao\FilesModel;
use Contao\Message;
use Contao\System;
use delahaye\GeoCode;
use Http\Adapter\Guzzle6\Client;
use Ivory\GoogleMap\Service\Geocoder\GeocoderService;
use Masterminds\HTML5\Parser\StringInputStream;
use Spatie\Geocoder\Geocoder;
use Spatie\SchemaOrg\Graph;

class ToolboxService
{

    /**
     * On Save Callback for coordinates
     * @param DataContainer $dc
     */
    public static function setCoordinates($dc=false, $strTable=false, $objItem=false)
    {

        if($dc && !$strTable){
            $strTable = $dc->table;
        }
        if (!$strTable || $strTable == '') {
            return;
        }

        if($dc && !$objItem){
            $objItem = $dc->activeRecord;
        }

        if(!$objItem){
            return;
        }

        Controller::loadDataContainer($strTable);
        $strModel = $GLOBALS['TL_MODELS'][$strTable];

        if ($strModel == '') {
            return;
        }

        $objDBItem = $strModel::findOneById($objItem->id);

        // Get Parent Model
        $strParentTable = $GLOBALS['TL_DCA'][$strTable]['config']['ptable'];
        $strParentModel = $GLOBALS['TL_MODELS'][$strParentTable];
        $objArchive = $strParentModel::findOneById($objItem->pid);
        $bolGenerateCoordinates = true;

        if(isset($objArchive->generateCoordinates)){
            if(!$objArchive->generateCoordinates){
                $bolGenerateCoordinates = false;
            }
        }

        if ($bolGenerateCoordinates && ($objItem->lat == '' || $objItem->long == '') && $objItem->address != '') {

            $arrCoordinates = false;

            if (class_exists('\delahaye\GeoCode')) {

                $strKey = Config::get('dlh_googlemaps_apikey');
                $strCoordinates = GeoCode::getCoordinates($objItem->address . $objItem->object_location, 'ch', 'de', $strKey);
                $arrCoordinates = explode(',', $strCoordinates);

            } elseif (class_exists('\Spatie\Geocoder\Geocoder')) {

                $strKey = Config::get('googlemaps_apiKey') ?? Config::get('dlh_googlemaps_apikey');

                if (!$strKey) {
                    // Add Backend system message
                    Message::addInfo('In den Contao Einstellungen wurde kein Google Maps API Key gefunden');
                    return;
                }

                $client = new \GuzzleHttp\Client();
                $geocoder = new Geocoder($client);
                $geocoder->setApiKey($strKey);
                $geocoder->setCountry('CH');
                $response = $geocoder->getCoordinatesForAddress($objItem->address);

                if(!$response){
                    Message::addError("Die Adresse konnte nicht gefunden werden. Keine Koordinaten generiert.");
                    return;
                }

                $arrCoordinates = array($response['lat'], $response['lng']);
            }

            if ($arrCoordinates && is_array($arrCoordinates)) {

                if (!is_null($arrCoordinates[0])) {
                    $objDBItem->lat = $arrCoordinates[0];
                }

                if (!is_null($arrCoordinates[1])) {
                    $objDBItem->long = $arrCoordinates[1];
                }

                // Add Backend system message
                if($dc){
                    Message::addConfirmation('Koordinaten für "'.$objDBItem->address.'" automatisch generiert.');
                }

                $objDBItem->save();
            }
        }
    }

    /**
     * Format Date for DCA-View
     * @param $value
     * @return false|int
     */
    public function loadDate($value)
    {
        return strtotime(date('Y-m-d', (int)$value) . ' 00:00:00');
    }

    public static function setJsonLd($JsonLdItem)
    {

        // Get Response Context and check it
        $objResponseContext = System::getContainer()->get('contao.routing.response_context_accessor')->getResponseContext();
        if (!$objResponseContext || !$objResponseContext->has(JsonLdManager::class)) {
            return;
        }

        // Get JsonLdManager
        $objJsonLdManager = $objResponseContext->get(JsonLdManager::class);

        // Check Input Parameter and transform (auto) if needed
        if (is_object($JsonLdItem)) {
            $objJsonLdItem = $JsonLdItem;
        } elseif (is_array($JsonLdItem)) {
            $objJsonLdItem = $objJsonLdManager->createSchemaOrgTypeFromArray($JsonLdItem);
        }

        // Add the Schema to the Graph (Response)
        $objJsonLdManager->getGraphForSchema(JsonLdManager::SCHEMA_ORG)->set($objJsonLdItem, Graph::IDENTIFIER_DEFAULT);

    }

    /**
     * Toggle alias and seo-fields, if no detail-page is set on the parent-archive
     * @param $strTablename
     */
    public static function toggleDetailFieldsFromPalette($strTablename)
    {
        $arrDetailPageIds = array();

        // Get the archive details
        if (array_key_exists('ptable', $GLOBALS['TL_DCA'][$strTablename]['config'])) {
            $strArchiveTable = $GLOBALS['TL_DCA'][$strTablename]['config']['ptable'];
            $strArchiveModel = $GLOBALS['TL_MODELS'][$strArchiveTable];

            // Try to get the jumpTo page ids
            $arrDetailPageIds = $strArchiveModel::getDetailPages(false);
        }

        // If no ids were found, there are now detailpages to be expected - so we can cleanup the palette
        if (count($arrDetailPageIds) == 0) {

            $objLanguageService = System::getContainer()->get('memo.foundation.language');

            $arrLanguages = $objLanguageService->getAllLanguages(true);

            foreach ($arrLanguages as $strLanguage) {
                $strPostfix = $objLanguageService->getLanguagePostfix($strLanguage);

                if ($strPostfix == '') {

                    $arrFieldsToRemove = array(
                        array('general_legend', 'alias'),
                        array('meta_legend', 'seo_title'),
                        array('meta_legend', 'seo_description'),
                        array('meta_legend', 'serpPreview'),
                    );

                } else {

                    $arrFieldsToRemove = array(
                        array('translation' . $strPostfix, 'alias' . $strPostfix),
                        array('translation' . $strPostfix, 'seo_title' . $strPostfix),
                        array('translation' . $strPostfix, 'seo_description' . $strPostfix),
                    );

                }

                foreach ($GLOBALS['TL_DCA'][$strTablename]['palettes'] as $strKey => $strPalette) {
                    if ($strKey == '__selector__') {
                        continue;
                    }

                    foreach ($arrFieldsToRemove as $arrFields) {
                        PaletteManipulator::create()
                            ->removeField($arrFields[1], $arrFields[0])
                            ->applyToPalette($strKey, $strTablename);
                    }
                }
            }
        }
    }

    public static function collectionToArray($objCollection, $bolResetKeys = false, $strField = 'title')
    {
        $arrCollection = array();

        if ($objCollection != null) {
            foreach ($objCollection as $objItem) {
                if ($bolResetKeys) {
                    $arrCollection[] = $objItem->$strField;
                } else {
                    $arrCollection[$objItem->id] = $objItem->$strField;
                }
            }
        }

        return $arrCollection;
    }

    public static function convertOrderString($strOrderBy, $strTable, $strOrderField=false)
    {
        $strSQL = '';

        switch ($strOrderBy) {
            case 'order_title':
                $strSQL = "$strTable.title";
                break;
            case 'order_title_desc':
                $strSQL = "$strTable.title DESC";
                break;
            case 'order_sorting':
                $strSQL = "$strTable.sorting";
                break;
            case 'order_sorting_desc':
                $strSQL = "$strTable.sorting DESC";
                break;
            case 'order_date':
                $strSQL = "$strTable.date";
                break;
            case 'order_date_desc':
                $strSQL = "$strTable.date DESC";
                break;
            case 'order_custom_field':
                $strSQL = "$strTable.$strOrderField";
                break;
            case 'order_custom_field_desc':
                $strSQL = "$strTable.$strOrderField DESC";
                break;
        }

        return $strSQL;
    }

    /**
     * Set page header meta by associative array.
     * @param array $arr
     * @return void
     */
    public static function setPageHeaderMeta(array $arr): void
    {
        if (System::getContainer()->has('contao.routing.response_context_accessor')) {
            $responseContext = System::getContainer()->get('contao.routing.response_context_accessor')->getResponseContext();
            $htmlHeadBag = $responseContext->get(HtmlHeadBag::class);

            if ($responseContext && $responseContext->has(HtmlHeadBag::class)) {
                foreach ($arr as $tag => $value) {
                    switch ($tag) {
                        case 'robots':
                            $htmlHeadBag->setMetaRobots($value);
                            break;
                        case 'title':
                            $htmlHeadBag->setTitle($value);
                            break;
                        case 'description':
                            $htmlHeadBag->setMetaDescription($value);
                            break;
                        case 'canonical':
                            $htmlHeadBag->setCanonicalUri($value);
                            break;
                    }
                }
            }
        } else {
            global $objPage;

            foreach ($arr as $tag => $value) {
                $objPage->{$tag} = $value;
            }
        }
    }

    public static function generateMarker($dc=false, $strTable=false, $objItem=false)
    {
        // Check object
        if (!$objItem || !isset($objItem->id)) {
            System::getContainer()->get('monolog.logger.contao.error')->error('No item given for marker generation');
            return;
        }

        // Get & Check Table
        if($dc && !$strTable){
            $strTable = $dc->table;
        }

        if (!$strTable || $strTable == '') {
            System::getContainer()->get('monolog.logger.contao.error')->error('No table given for marker generation');
            return;
        }

        // Get and check Class/Model
        Controller::loadDataContainer($strTable);
        $strModel = $GLOBALS['TL_MODELS'][$strTable];

        if ($strModel == '' || !class_exists($strModel)) {
            System::getContainer()->get('monolog.logger.contao.error')->error('No model found for table ' . $strTable);
            return;
        }

        // Get Item
        $objDBItem = $strModel::findOneById($objItem->id);

        if (!$objDBItem) {
            System::getContainer()->get('monolog.logger.contao.error')->error('No item found for table ' . $strTable . ' with id ' . $dc->activeRecord->id);
            return;
        }

        // Get Parent / Archive
        $objArchive = $objDBItem->getRelated('pid');

        if (!$objArchive) {
            System::getContainer()->get('monolog.logger.contao.error')->error('No archive found for table ' . $strTable . ' with id ' . $objItem->id);
            return;
        }

        // Check data
        if (!$objArchive->addMarker || !$objArchive->markerSRC || $objArchive->markerSRC == '') {
            System::getContainer()->get('monolog.logger.contao.error')->error('No marker found for table ' . $strTable . ' with id ' . $objItem->id);
            return;
        }

        if (!$objDBItem->singleSRC || $objDBItem->singleSRC == '') {
            System::getContainer()->get('monolog.logger.contao.error')->error('No image found for item: table ' . $strTable . ' with id ' . $objItem->id);
            return;
        }

        // Get Item Image
        $objItemImage = FilesModel::findByUuid($objDBItem->singleSRC);

        // Check File
        if (!$objItemImage || file_exists($objItemImage->path) === false) {
            System::getContainer()->get('monolog.logger.contao.error')->error('Defined image not found for item: table ' . $strTable . ' with id ' . $objItem->id);
            return;
        }

        // Get Template Image
        $objTemplateImage = FilesModel::findByUuid($objArchive->markerSRC);

        // Check File
        if (!$objTemplateImage || file_exists($objTemplateImage->path) === false) {
            System::getContainer()->get('monolog.logger.contao.error')->error('Defined marker background image not found for item: table ' . $strTable . ' with id ' . $objItem->id);
            return;
        }

        // Check the destination
        $objMarkerFolder = FilesModel::findByUuid($objArchive->markerFolder);

        if (!$objMarkerFolder || file_exists($objMarkerFolder->path) === false) {
            System::getContainer()->get('monolog.logger.contao.error')->error('Defined marker folder not found');

            return;
        }

        // Create Base from Original Image
        switch ($objItemImage->extension) {
            case 'jpeg':
            case 'jpg':
                $objItemImageSource = imagecreatefromjpeg($objItemImage->path);
                break;
            case 'png':
                $objItemImageSource = imagecreatefrompng($objItemImage->path);
                break;
            case 'gif':
                $objItemImageSource = imagecreatefromgif($objItemImage->path);
                break;
            default:
                System::getContainer()->get('monolog.logger.contao.error')->error('Image type not supported for marker generation: ' . $objItemImage->extension);
                return;
        }

        // Define  filename, path etc.
        if ($objDBItem->alias != '') {
            $strFilename = 'marker-' . $objDBItem->alias . '-' . $objDBItem->id . '.png';
        } else {
            $strFilename = 'marker-' . $objDBItem->id . '.png';
        }
        $strFilepath = $objMarkerFolder->path . '/' . $strFilename;

        // Get Image Size
        list($intSourceWidth, $intSourceHeight) = getimagesize($objItemImage->path);

        // Load Marker Template
        switch ($objTemplateImage->extension) {
            case 'jpeg':
            case 'jpg':
                $objMarker = imagecreatefromjpeg($objTemplateImage->path);
                break;
            case 'png':
                $objMarker = imagecreatefrompng($objTemplateImage->path);
                break;
            case 'gif':
                $objMarker = imagecreatefromgif($objTemplateImage->path);
                break;
            default:
                System::getContainer()->get('monolog.logger.contao.error')->error('Image type not supported for marker generation: ' . $objTemplateImage->extension);
                return;
        }
        imagesavealpha($objMarker, true);

        // Get Marker Sizes
        $intMarkerWidth = imagesx($objMarker);
        $intMarkerHeight = imagesy($objMarker);

        // Create new Image and crop on Item Image
        $objMarkerImage = imagecreatetruecolor($intMarkerWidth, $intMarkerHeight);
        imagealphablending($objMarkerImage, false);
        imagecopyresampled($objMarkerImage, $objItemImageSource, 0, 0, 0, 0, $intMarkerWidth - $objArchive->markerWidthTrim, $intMarkerHeight - $objArchive->markerHeightTrim, $intSourceWidth, $intSourceHeight);

        // Merge Item Image into Marker Image with offset (for border)
        imagecopyresampled($objMarker, $objMarkerImage, $objArchive->markerOffsetX, $objArchive->markerOffsetY, 0, 0, $intMarkerWidth - $objArchive->markerWidthTrim, $intMarkerHeight - $objArchive->markerHeightTrim, $intMarkerWidth - $objArchive->markerWidthTrim, $intMarkerHeight - $objArchive->markerHeightTrim);

        // Write Marker to Folder
        imagepng($objMarker, $strFilepath);

        if (!file_exists($strFilepath)) {
            System::getContainer()->get('monolog.logger.contao.error')->error('Marker could not be created');
            return;
        }

        Dbafs::addResource($strFilepath);

        // Get Marker Object
        $objMarkerFile = FilesModel::findByPath($strFilepath);

        // Check Marker Object
        if (!$objMarkerFile) {
            System::getContainer()->get('monolog.logger.contao.error')->error('Marker could not be added to database');
            return;
        }

        // Update Item
        $objDBItem->markerSRC = $objMarkerFile->uuid;
        $objDBItem->save();

    }

    public static function stringToHexColor($text,$min_brightness=100,$spec=10)
    {
        // Check inputs
        if(!is_int($min_brightness)) throw new Exception("$min_brightness is not an integer");
        if(!is_int($spec)) throw new Exception("$spec is not an integer");
        if($spec < 2 or $spec > 10) throw new Exception("$spec is out of range");
        if($min_brightness < 0 or $min_brightness > 255) throw new Exception("$min_brightness is out of range");


        $hash = md5($text);  //Gen hash of text
        $colors = array();
        for($i=0;$i<3;$i++)
            $colors[$i] = max(array(round(((hexdec(substr($hash,$spec*$i,$spec)))/hexdec(str_pad('',$spec,'F')))*255),$min_brightness)); //convert hash into 3 decimal values between 0 and 255

        if($min_brightness > 0)  //only check brightness requirements if min_brightness is about 100
            while( array_sum($colors)/3 < $min_brightness )  //loop until brightness is above or equal to min_brightness
                for($i=0;$i<3;$i++)
                    $colors[$i] += 10;	//increase each color by 10

        $output = '';

        for($i=0;$i<3;$i++)
            $output .= str_pad(dechex((int)$colors[$i]),2,'',STR_PAD_LEFT);  //convert each color to hex and append to output

        return $output;
    }

    public static function getContrastColor($hexColor)
    {
        // hexColor RGB
        $R1 = hexdec(substr($hexColor, 1, 2));
        $G1 = hexdec(substr($hexColor, 3, 2));
        $B1 = hexdec(substr($hexColor, 5, 2));

        // Black RGB
        $blackColor = "#000000";
        $R2BlackColor = hexdec(substr($blackColor, 1, 2));
        $G2BlackColor = hexdec(substr($blackColor, 3, 2));
        $B2BlackColor = hexdec(substr($blackColor, 5, 2));

        // Calc contrast ratio
        $L1 = 0.2126 * pow($R1 / 255, 2.2) +
            0.7152 * pow($G1 / 255, 2.2) +
            0.0722 * pow($B1 / 255, 2.2);

        $L2 = 0.2126 * pow($R2BlackColor / 255, 2.2) +
            0.7152 * pow($G2BlackColor / 255, 2.2) +
            0.0722 * pow($B2BlackColor / 255, 2.2);

        $contrastRatio = 0;
        if ($L1 > $L2) {
            $contrastRatio = (int)(($L1 + 0.05) / ($L2 + 0.05));
        } else {
            $contrastRatio = (int)(($L2 + 0.05) / ($L1 + 0.05));
        }

        // If contrast is more than 5, return black color
        if ($contrastRatio > 5) {
            return '#000000';
        } else {
            // if not, return white color.
            return '#FFFFFF';
        }
    }

    public static function getItemModelByModule($strModule){

        // Case: Team
        if(stripos($strModule, 'team') !== false){
            return 'Memo\TeamBundle\Model\TeamModel';
        }

        // Case: Product
        if(stripos($strModule, 'product') !== false){
            return 'Memo\ProductBundle\Model\ProductModel';
        }

        // Case: Portfolio
        if(stripos($strModule, 'portfolio') !== false){
            return 'Memo\PortfolioBundle\Model\PortfolioModel';
        }

        // Case: Jobs
        if(stripos($strModule, 'job') !== false){
            return 'Memo\JobBundle\Model\JobModel';
        }

        return false;

    }

    public static function getFieldsByModel($strModel, $bolAsOptions=true){

        if( !in_array($strModel, $GLOBALS['TL_MODELS'])){
            return array();
        }

        $arrFields = array();
        foreach($GLOBALS['TL_MODELS'] as $strTable => $strDCAModel){
            if($strDCAModel == $strModel){

                // Load DCA and Language for this table
                Controller::loadDataContainer($strTable);
                System::loadLanguageFile($strTable);
                $arrFields = $GLOBALS['TL_DCA'][$strTable]['fields'];
            }
        }

        $arrReturn = array();
        foreach($arrFields as $strFieldName => $arrField){
            if(isset($arrField['label'][0])){
                $arrReturn[$strFieldName] = $strFieldName . ' (' .$arrField['label'][0] . ')';
            } else {
                $arrReturn[$strFieldName] =$strFieldName;
            }
        }

        return $arrReturn;

    }

    public static function getTableByModel($strModel){

        if( !in_array($strModel, $GLOBALS['TL_MODELS'])){
            return false;
        }

        foreach($GLOBALS['TL_MODELS'] as $strTable => $strDCAModel){
            if($strDCAModel == $strModel){
                return $strTable;
            }
        }

        return false;

    }

    public static function isStringSerialized($strString){
        return is_string($strString) && substr($strString, 0, 2) === 'a:';
    }
}
