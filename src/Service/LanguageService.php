<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\FoundationBundle\Service;

use Contao\PageModel;
use Contao\System;
use Contao\Config;

class LanguageService
{

    public static function getAllLanguages($bolIncludeCurrent = true, $bolOnlyActiveLanguages = false, $bolLabels = false)
    {

        // Presets
        $strDefaultLanguage = self::getDefaultLanguage();
        $arrLanguages = array();

        if ($bolOnlyActiveLanguages) {

            // Get all Languages by fetching Root-Pages (has to be published)
            $colRootPages = PageModel::findBy(array('type=?', 'language!=?', 'published=?'), array('root', "", 1));
            $objRootPage = PageModel::findOneBy(array('type=?', 'language!=?', 'fallback=?'), array('root', "", 1));

        } else {

            // Get all Languages by fetching Root-Pages (does not have to be published)
            $colRootPages = PageModel::findBy(array('type=?', 'language!=?'), array('root', ""));
            $objRootPage = PageModel::findOneBy(array('type=?', 'language!=?', 'fallback=?'), array('root', "", 1));

        }

        // Check if languages were defined in the page-tree
        if (!$objRootPage || !$colRootPages) {
            throw new \Exception("No root-page with a language or no fallback language defined");
        }

        // Add the languages to a array
        while ($colRootPages->next()) {
            if (!in_array($colRootPages->languag, $arrLanguages)) {
                if($bolLabels){
                    $arrLanguages[$colRootPages->language] = LanguageService::getLanguageLabel($colRootPages->language);
                } else {
                    $arrLanguages[$colRootPages->language] = $colRootPages->language;
                }

            }
        }

        // Remove current language - if widhed for
        if (!$bolIncludeCurrent) {
            foreach ($arrLanguages as $intKey => $strLanguage) {
                if ($strDefaultLanguage == $intKey) {
                    unset($arrLanguages[$intKey]);
                }
            }

        }

        // Return result
        return $arrLanguages;
    }

    /**
     * Generate DCA-Fields for translations
     * @param $strTable
     * @throws \Exception
     */
    public function generateTranslateDCA($strTable)
    {
        $strFallbackLanguage = self::getDefaultLanguage();
        $arrLanguages = self::getAllLanguages();

        // Loop all Languages
        foreach ($arrLanguages as $strLanguage) {

            // Reset the palette string
            unset($arrTranslatedFields);

            // Detect Default language and "jump" it
            if ($strLanguage == $strFallbackLanguage) {
                continue;
            }

            // Define Postfix and Language Values
            $strPostfix = "_" . $strLanguage;

            // Loop all DCA-fields
            if (!array_key_exists($strTable, $GLOBALS['TL_DCA']) || is_null($GLOBALS['TL_DCA'][$strTable])) {
                continue;
            }

            foreach ($GLOBALS['TL_DCA'][$strTable]['fields'] as $strFieldName => $arrFieldValues) {

                // Detect fields, that where marked as "translate"
                if (array_key_exists('translate', $arrFieldValues) && $arrFieldValues['translate']) {

                    unset($arrNewField);

                    // Add the fieldname to the array of fieldnames
                    $arrTranslatedFields[] = $strFieldName . $strPostfix;

                    // Create a new field-array and clear all values, not needed in the translation
                    $arrNewField = $arrFieldValues;
                    $arrNewField['translate'] = false;
                    $arrNewField['generated'] = true;
                    $arrNewField['filter'] = false;
                    $arrNewField['search'] = false;
                    $arrNewField['sort'] = false;
                    $arrNewField['eval']['mandatory'] = false;
                    unset($arrNewField['save_callback']);

                    // Enable the new language-fields to be manipulated before we return them to the array
                    if (isset($GLOBALS['TL_HOOKS']['prepareTranslatedField']) && is_array($GLOBALS['TL_HOOKS']['prepareTranslatedField'])) {
                        foreach ($GLOBALS['TL_HOOKS']['prepareTranslatedField'] as $callback) {
                            $this->import($callback[0]);
                            $arrNewField = $this->$callback[0]->$callback[1]($arrNewField, $arrFieldValues, $strPostfix, $strTable);
                        }
                    }

                    // Save the new field to the array of fields
                    $GLOBALS['TL_DCA'][$strTable]['fields'][$strFieldName . $strPostfix] = $arrNewField;

                }

            }

            // Build the palette string for the new language
            $strNewPalette = '{translation' . $strPostfix . ':hide}, ';
            if (is_array($arrTranslatedFields)) {
                $strNewPalette .= implode(', ', $arrTranslatedFields);
            }
            $strNewPalette .= ';';

            // Add the new palette-string to the existing palettes
            $bolPaletteFound = false;
            foreach ($GLOBALS['TL_DCA'][$strTable]['palettes'] as $strPaletteName => $strPalette) {

                if ($strPaletteName != '__selector__' && preg_match('/{.*}/', $strPalette)) {

                    preg_match_all('/{(.*?)}/mi', $strPalette, $arrMatches);
                    $strLastlegend = end($arrMatches[1]);

                    if ($strLastlegend != '') {
                        $bolPaletteFound = true;
                        $GLOBALS['TL_DCA'][$strTable]['palettes'][$strPaletteName] = str_replace('{' . $strLastlegend . '}', $strNewPalette . '{' . $strLastlegend . '}', $GLOBALS['TL_DCA'][$strTable]['palettes'][$strPaletteName]);
                    }
                }
            }

            if (!$bolPaletteFound) {
                throw new \Exception('No "publish_legend" found in the palette for the table: ' . $strTable);
            }

            // Add the legend.
            $GLOBALS['TL_LANG'][$strTable]['translation' . $strPostfix] = 'Übersetzungen ' . LanguageService::getLanguageLabel($strLanguage);
        }
    }


    /**
     * Returns the cleaned postfix string, that is added to dca-fields for multilanguage content
     * @param    $strLanguage    Language (de, de_CH, en, fr, etc)
     * @return $strPostfix        Language-Postfix (de, dech, en, fr, etc.)
     **/
    public static function getLanguagePostfix(string $strLanguage)
    {

        // Get Default Language
        $strDefaultLanguage = System::getContainer()->get('memo.foundation.language')->getDefaultLanguage();

        // Cleanup Language-String
        $strPostfix = strtolower($strLanguage);
        $strPostfix = str_replace('_', '', $strPostfix);
        $strPostfix = str_replace('-', '', $strPostfix);
        $strPostfix = str_replace(' ', '', $strPostfix);

        // Detect if the Default-Language is wanted
        if ($strPostfix == $strDefaultLanguage) {
            $strPostfix = '';
        } else {
            $strPostfix = '_' . $strPostfix;
        }

        // Return the postfix-string
        return $strPostfix;
    }

    public static function getDefaultLanguage()
    {
        $strDefaultLanguage = Config::get('default_language');

        if (!$strDefaultLanguage) {
            $strDefaultLanguage = 'de';
        }

        return $strDefaultLanguage;
    }

    public static function getLanguageLabel($strLanguageCode) : string
    {

        // Get service contao.intl.locales
        $objLocales = System::getContainer()->get('contao.intl.locales');

        // Get the language label
        if($arrLanguageLabels = $objLocales->getLanguages()){
            if(array_key_exists($strLanguageCode, $arrLanguageLabels)) {
                $strLanguageCode = $arrLanguageLabels[$strLanguageCode];
            }
        } elseif(is_string($strLanguageCode)){
            $strLanguageCode = strtoupper($strLanguageCode);
        }

        // Return the language code if no label was found
        return $strLanguageCode;


    }

    public static function loadTranslationPlaceholders($dc)
    {
        // Load the dca for this field
        $strTable = $dc->table;
        $strField = $dc->field;
        $bolPlaceholder = false;

        // Load the dca for this table
        \Contao\Controller::loadDataContainer($strTable);
        if(!isset($GLOBALS['TL_DCA'][$strTable]['fields'])){
            return;
        }
        $arrDCAFields = $GLOBALS['TL_DCA'][$strTable]['fields'];

        // Get all languages
        $arrLanguages = self::getAllLanguages(false);

        // Get the model from this table
        $strModel = $GLOBALS['TL_MODELS'][$strTable];

        if(!$strModel){
            return;
        }

        $objItem = $strModel::findByPk($dc->id);

        if(!$objItem){
            return;
        }

        foreach($arrDCAFields as $strFieldName => $arrDCA){

            if(!isset($arrDCA['inputType']) || in_array($arrDCA['inputType'], ['fileTree', 'checkbox', 'radio', 'hidden', 'password', 'upload', 'select', 'checkboxWizard'])){
                continue;
            }

            $bolPlaceholder = false;

            // Loop all languages
            foreach($arrLanguages as $strLanguage) {

                // Get the language postfix
                $strPostfix = self::getLanguagePostfix($strLanguage);

                // Remove this postfix from the end of the fieldname
                $strFieldNameOriginal = preg_replace('/'.$strPostfix.'$/', '', $strFieldName);

                // Get the original value
                $strOriginalValue = $objItem->{$strFieldNameOriginal};

                if($strOriginalValue && $strOriginalValue != ''){
                    $bolPlaceholder = true;
                    break;
                }
            }

            if($bolPlaceholder){

                if(isset($arrDCA['inputType']) && in_array($arrDCA['inputType'], ['textarea']) && isset($arrDCA['eval']['rte']) && $arrDCA['eval']['rte'] == 'tinyMCE'){
                    $strOriginalValue = "[OHNE HTML] " .strip_tags((string)$strOriginalValue);
                }

                // Update the placeholder of this field
                $GLOBALS['TL_DCA'][$strTable]['fields'][$strFieldName]['eval']['placeholder'] = $strOriginalValue;

            }

        }
    }
}
