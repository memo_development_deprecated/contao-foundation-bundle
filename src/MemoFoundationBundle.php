<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\FoundationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MemoFoundationBundle extends Bundle
{
}
