<?php

declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_settings']['memo_legend'] = 'Media Motion AG - Einstellungen';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_settings']['fields']['default_language'] = array('Standardsprache', 'Definieren Sie hier die Standardsprache, z.B. de, en, fr, etc. Leer = de');
$GLOBALS['TL_LANG']['tl_settings']['fields']['googlemaps_apiKey'] = array('Google Maps API Key', 'Definieren Sie hier den Google Maps API Key, um die Google Maps Funktionalität zu nutzen.');
