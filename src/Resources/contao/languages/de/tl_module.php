<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

$GLOBALS['TL_LANG']['tl_module']['memo_foundation'] = array("Foundation Module", "Frontend Module die von der Media Motion AG entwickelt wurden.");


$GLOBALS['TL_LANG']['tl_module']['filter_legend'] = "Frontend Filter";
$GLOBALS['TL_LANG']['tl_module']['detail_legend'] = "Detailseite";


$GLOBALS['TL_LANG']['tl_module']['foundation_archives'] = array("Archive", "Aus welchen Archiven sollen die Elemente ausgelesen werden?");
$GLOBALS['TL_LANG']['tl_module']['foundation_item_selection'] = array("Element Auswahl", "Welche Elemente sollen ausgelesen werden?");
$GLOBALS['TL_LANG']['tl_module']['foundation_order'] = array("Sortierung", "Wonach soll sortiert werden?");
$GLOBALS['TL_LANG']['tl_module']['foundation_order_field'] = array("Sortierfeld", "Welches Feld soll für die Sortierung verwendet werden?");
$GLOBALS['TL_LANG']['tl_module']['foundation_order_field_direction'] = array("Sortierreihenfolge", "Soll auf oder absteigend sortiert werden?");

$GLOBALS['TL_LANG']['tl_module']['foundation_item_template'] = array("Element-Template", "Einzel-Element Template für das Kindelement des Templates");
$GLOBALS['TL_LANG']['tl_module']['imgSize'] = array("Bildgrösse (Einzelbild)", "Bilgrösse aller 'singleSRC' Felder");
$GLOBALS['TL_LANG']['tl_module']['imgSizeGallery'] = array("Bildgrösse (Galerie)", "Bildgrösse aller 'multiSRC' Felder");
$GLOBALS['TL_LANG']['tl_module']['jumpToOverride'] = array("Detailseite überschreiben", "Hier haben Sie die Möglichkeit, die Detailseite der Archive zu überschreiben.");
$GLOBALS['TL_LANG']['tl_module']['sql_filter'] = array("SQL Filter", "Ergänzen Sie die oben definierten Filter mit einem SQL Filter. (z.B. 'tl_memo_portfolio.featured=1')");
$GLOBALS['TL_LANG']['tl_module']['foundation_featured'] = array("Hervorgehobene filtern", "Sollen die Einträge nach Feld 'hervorheben' gefiltert werden?");

$GLOBALS['TL_LANG']['tl_module']['only_active_values'] = array('Nur aktive Werte', 'Sollen nur Filter-Optionen angezeigt werden, die auch in Verwendung sind? (längere Ladezeit)');
$GLOBALS['TL_LANG']['tl_module']['attribute_selection'] = array('Nur ausgewählte Filter ausgeben', 'Welche Filter sollen angezeigt werden? Leer = alle veröffentlichten Filter');
$GLOBALS['TL_LANG']['tl_module']['form_placeholders'] = array('Platzhalter anstatt Labels', 'Sollen die Filter-Labels durch Platzhalter ersetzt werden?');
$GLOBALS['TL_LANG']['tl_module']['add_blank_option'] = array('Titel / Leere Option hinzufügen', 'Soll den Dropdown-Menus eine leere Option zur Auswahl aller Filterwerte hinzugefügt werden?');
$GLOBALS['TL_LANG']['tl_module']['add_reset_button'] = array('Reset-Button hinzufügen', 'Soll dem Formular ein Reset-Button hinzugefügt werden?');

$GLOBALS['TL_LANG']['tl_module']['foundation_orders']['order_title'] = "nach Titel";
$GLOBALS['TL_LANG']['tl_module']['foundation_orders']['order_title_desc'] = "nach Titel (absteigend)";
$GLOBALS['TL_LANG']['tl_module']['foundation_orders']['order_sorting'] = "nach Sortierung im Backend";
$GLOBALS['TL_LANG']['tl_module']['foundation_orders']['order_sorting_desc'] = "nach Sortierung im Backend (absteigend)";
$GLOBALS['TL_LANG']['tl_module']['foundation_orders']['order_date'] = "nach Datum (altes zuerst)";
$GLOBALS['TL_LANG']['tl_module']['foundation_orders']['order_date_desc'] = "nach Datum (neues zuerst)";
$GLOBALS['TL_LANG']['tl_module']['foundation_orders']['order_custom_field'] = "nach benutzerdefiniertem Feld";
$GLOBALS['TL_LANG']['tl_module']['foundation_orders']['order_custom_field_desc'] = "nach benutzerdefiniertem Feld (absteigend)";

$GLOBALS['TL_LANG']['tl_module']['foundation_feature']['all'] = "Alle anzeigen";
$GLOBALS['TL_LANG']['tl_module']['foundation_feature']['only_featured'] = "Nur hervorgehobene";
$GLOBALS['TL_LANG']['tl_module']['foundation_feature']['only_not_featured'] = "Nur nicht hervorgehobene";
