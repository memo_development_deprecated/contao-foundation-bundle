<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\ContentModel;
use Contao\Controller;
use Contao\System;
use Contao\Backend;

Controller::loadDataContainer('tl_module');
System::loadLanguageFile('tl_module');


$GLOBALS['TL_DCA']['tl_content']['fields']['sql_filter'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['sql_filter'];

$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_archives'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_archives'];
$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_archives']['eval']['submitOnChange'] = true;

$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_item_template'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_template'];
$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_item_selection'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_selection'];

$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_order'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_order'];
$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_order_field'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_order_field'];
$GLOBALS['TL_DCA']['tl_content']['palettes']['__selector__'][] = 'foundation_order';
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['foundation_order_order_custom_field'] = 'foundation_order_field';
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['foundation_order_order_custom_field_desc'] = 'foundation_order_field';

$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_featured'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_featured'];
