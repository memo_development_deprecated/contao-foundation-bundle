<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Add palettes to tl_module
 */

use Contao\Backend;
use Contao\Controller;
use Contao\System;
use Contao\BackendUser;
use Contao\DataContainer;
use Memo\FoundationBundle\Service\ToolboxService;

$GLOBALS['TL_DCA']['tl_module']['fields']['imgSize']['label'] = &$GLOBALS['TL_LANG']['tl_module']['imgSize'];
$GLOBALS['TL_DCA']['tl_module']['fields']['imgSizeGallery'] = $GLOBALS['TL_DCA']['tl_module']['fields']['imgSize'];
$GLOBALS['TL_DCA']['tl_module']['fields']['imgSizeGallery']['label'] = &$GLOBALS['TL_LANG']['tl_module']['imgSizeGallery'];

// Add Subpalette to add foundation_archive_order_field if order_custom_field or order_custom_field_desc is selected
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'foundation_order';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['foundation_order_order_custom_field'] = 'foundation_order_field';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['foundation_order_order_custom_field_desc'] = 'foundation_order_field';

$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_order'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['foundation_order'],
    'exclude' => true,
    'inputType' => 'select',
    'options' => array(
        'order_title',
        'order_title_desc',
        'order_sorting',
        'order_sorting_desc',
        'order_date',
        'order_date_desc',
        'order_custom_field',
        'order_custom_field_desc',
    ),
    'reference' => &$GLOBALS['TL_LANG']['tl_module']['foundation_orders'],
    'eval' => array('tl_class' => 'w50 clr', 'submitOnChange' => true),
    'sql' => "varchar(32) NOT NULL default 'order_sorting'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_order_field'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['foundation_order_field'],
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_module_foundation', 'getFields'),
    'eval' => array('tl_class' => 'w50', 'includeBlankOption' => true, 'mandatory' => true),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_featured'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['foundation_featured'],
    'exclude' => true,
    'inputType' => 'select',
    'options' => array(
        'all',
        'only_featured',
        'only_not_featured'
    ),
    'reference' => &$GLOBALS['TL_LANG']['tl_module']['foundation_feature'],
    'eval' => array('tl_class' => 'w50 clr'),
    'sql' => "varchar(32) NOT NULL default 'all'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_archives'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['foundation_archives'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'options_callback' => array('tl_module_foundation', 'getArchives'),
    'eval' => array(
        'multiple' => true,
        'mandatory' => true,
        'tl_class' => 'clr'
    ),
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_template'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['foundation_item_template'],
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_module_foundation', 'getItemTemplates'),
    'eval' => array(
        'tl_class' => 'w50'
    ),
    'sql' => "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_selection'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['foundation_item_selection'],
    'exclude' => true,
    'inputType' => 'checkboxWizard',
    'options_callback' => array('tl_module_foundation', 'getOptions'),
    'eval' => array(
        'multiple' => true,
        'tl_class' => 'clr'
    ),
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['sql_filter'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['sql_filter'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array(
        'tl_class' => 'long clr',
        'decodeEntities' => true,
    ),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['attribute_selection'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['attribute_selection'],
    'exclude' => true,
    'inputType' => 'checkboxWizard',
    'foreignKey' => 'tl_memo_job_attribute.title',
    'eval' => array('multiple' => true, 'tl_class' => 'clr'),
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['only_active_values'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['only_active_values'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array('tl_class' => 'w50'),
    'sql' => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['add_blank_option'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['add_blank_option'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array('tl_class' => 'w50'),
    'sql' => "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['add_reset_button'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['add_reset_button'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array('tl_class' => 'w50'),
    'sql' => "char(1) NOT NULL default '0'"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['form_placeholders'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['form_placeholders'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array('tl_class' => 'w50'),
    'sql' => "char(1) NOT NULL default ''"
);

/**
 * Class tl_module_foundation
 */
class tl_module_foundation extends Backend
{

    public function getArchives(DataContainer $dc)
    {
        return array();
    }

    public function getItemTemplates(DataContainer $dc)
    {
        return $this->getTemplateGroup('foundation_');
    }

    public function getOptions(DataContainer $dc)
    {
        return array();
    }

    public function getFields(DataContainer $dc)
    {
        if (!$dc->activeRecord->type) {
            return array();
        }

        // Get Item Model from current foundation Bundle
        $strItemModel = ToolboxService::getItemModelByModule($dc->activeRecord->type);
        return ToolboxService::getFieldsByModel($strItemModel);

    }
}
