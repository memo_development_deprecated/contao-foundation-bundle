<?php

declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

PaletteManipulator::create()
    ->addLegend('memo_legend', 'tl_settings', PaletteManipulator::POSITION_AFTER)
    ->addField('default_language', 'memo_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_settings');

$GLOBALS['TL_DCA']['tl_settings']['fields']['default_language'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_settings']['fields']['default_language'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array('tl_class' => 'clr', 'mandatory' => false),
);

// Check if a google maps api key already exists
if (!array_key_exists('dlh_googlemaps_apikey', $GLOBALS['TL_DCA']['tl_settings']['fields']) && !array_key_exists('googlemaps_apiKey', $GLOBALS['TL_DCA']['tl_settings']['fields'])) {


    PaletteManipulator::create()
        ->addLegend('memo_legend', 'tl_settings', PaletteManipulator::POSITION_AFTER)
        ->addField('googlemaps_apiKey', 'memo_legend', PaletteManipulator::POSITION_APPEND)
        ->applyToPalette('default', 'tl_settings');


    $GLOBALS['TL_DCA']['tl_settings']['fields']['googlemaps_apiKey'] = array(
        'label' => &$GLOBALS['TL_LANG']['tl_settings']['fields']['googlemaps_apiKey'],
        'exclude' => true,
        'inputType' => 'text',
        'eval' => array('tl_class' => 'clr long', 'mandatory' => false),
    );

}

