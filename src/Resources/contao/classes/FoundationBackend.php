<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\MemoFoundationBundle\Classes;

use Ausi\SlugGenerator\SlugGenerator;
use Ausi\SlugGenerator\SlugOptions;
use Contao\Backend;
use Contao\Database;
use Contao\DC_Table;
use Contao\Input;
use Contao\StringUtil;
use Contao\Versions;
use Contao\Image;
use Contao\System;
use Contao\PageModel;
use Memo\CategoryBundle\Model\CategoryModel;
use Memo\FoundationBundle\Service\ToolboxService;

/**
 * Class FoundationBackend
 */
class FoundationBackend extends Backend
{

    public function generateSerpUrl($objItem, $bolArchiveAlias = true, $strDetailPageAlias = '')
    {
        // Get the Url of the item
        return $objItem->getURL();
    }

    /**
     * @param $value        string Date
     * @return string
     */
    public function loadDate($value)
    {
        return strtotime(date('Y-m-d', $value) . ' 00:00:00');
    }

    /**
     * Returns the cleaned postfix string, that is added to dca-fields for multilanguage content
     * @param    $dc                DataContainer Object
     * @param    $strTablename    String tablename (tl_memo_xy)
     * @param    $strValue        String new value for alias
     * @param    $arrFields        Array which defines, which field(s) should be used, to generate the alias
     * @param    $strPrefix        String which defines, what should be prepended, to the field-value / alias
     * @param    $strPostfix        String which defines, what should be appended, to the field-value / alias
     * @param    $strFallbackPrefix        String which is used to prefix a alias, if it is in conflict with predefined aliases
     * @return $strAlias        String the new alias
     **/
    public function generateAliasFoundation(DC_Table $dc, string $strValue, string $strTablename, array $arrFields = array('title'), string $strPrefix = '', string $strPostfix = '', string $strFallbackPrefix = 'alias-')
    {
        // Only set a new alias, it there is none set
        if ($strValue != '') {
            return $strValue;
        }
        // Define the Options for the slug-generator
        $objGenerator = new SlugGenerator((new SlugOptions)
            ->setValidChars('a-z0-9')
            ->setLocale('de')
            ->setDelimiter('-')
        );

        // Generate Alias
        $strFieldValues = "";
        foreach ($arrFields as $strField) {
            $strFieldValues .= $dc->activeRecord->$strField . "-";
        }
        $strFieldValues = rtrim($strFieldValues, "-");
        $strValue = $objGenerator->generate($strPrefix . $strFieldValues . $strPostfix);

        // Add a prefix to reserved names (see #6066)
        if (in_array($strValue, array('top', 'wrapper', 'header', 'container', 'main', 'left', 'right', 'footer'))) {
            $strValue = $strFallbackPrefix . $strValue;
        }

        // Check if the Alias already exists
        $objDatabase = Database::getInstance();
        $objAlias = $objDatabase->prepare("SELECT id FROM $strTablename WHERE id=? OR alias=?")->execute($dc->id, $strValue);

        if ($objAlias->numRows > 1) {
            $strValue .= '-' . $dc->id;
        }

        // Return the alias
        return $strValue;
    }

    /**
     * @param $intId        Int ID of the entry to toggle
     * @param $blnVisible    Boolean Visible or not?
     * @param $dc            DataContainer (if exists)
     * @param $strTable        String Table in which the Field should be toggled in
     * @param $strField        String Fieldname to be toggled
     */
    public function toggleVisibilityFoundation($intId, $blnVisible, $dc, $strTable, $strField)
    {

        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        $objVersions = new Versions($strTable, $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (array_key_exists('save_callback', $GLOBALS['TL_DCA'][$strTable]['fields'][$strField]) && is_array($GLOBALS['TL_DCA'][$strTable]['fields'][$strField]['save_callback'])) {
            foreach ($GLOBALS['TL_DCA'][$strTable]['fields'][$strField]['save_callback'] as $callback) {
                if (is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, ($dc ?: $this));
                } elseif (is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, ($dc ?: $this));
                }
            }
        }

        // Update the database
        $objDatabase = Database::getInstance();
        $objDatabase->prepare("UPDATE " . $strTable . " SET tstamp=" . time() . ", " . $strField . "='" . ($blnVisible ? '1' : '') . "' WHERE id=?")->execute($intId);
        $objVersions->create();
    }

    /**
     * Returns the cleaned postfix string, that is added to dca-fields for multilanguage content
     * @param    $dc                DataContainer Object
     * @param    $strField        String fieldname which should be toggled
     * @param    $strIcon        String iconame for when toggled (from contao icons, without .svg/.gif)
     * @param    $strIconCurrent    String iconame of the default icon (from contao icons)
     * @param    $strTitle        String the title for this action-button
     * @param    $strAttributes    String the attributes for this action-button
     * @param    $strLabel        String the label for this action-button
     * @return $strAlias        String the url-markup
     **/
    public function toggleIconFoundation(array $row, string $strField, string $strIcon, string $strIconCurrent, string $strTitle, string $strAttributes, string $strLabel)
    {
        if (Input::get('tid')) {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), null);
            $this->redirect($this->getReferer());
        }

        // Define the new link
        $strHrefPostfix = '&amp;tid=' . $row['id'] . '&amp;state=' . ($row[$strField] ? '' : 1);
        $strHref = Backend::addToUrl($strHrefPostfix);

        // If the field was disabled unti. now, change the icon
        if (!$row[$strField]) {
            $strIcon = $strIcon . '.svg';
        } else {
            $strIcon = $strIconCurrent;
        }

        $strMarkup = '<a href="' . $strHref . '" title="' . StringUtil::specialchars($strTitle) . '"' . $strAttributes . '>' . Image::getHtml($strIcon, $strLabel, 'data-state="' . ($row[$strField] ? 1 : 0) . '"') . '</a> ';

        // Return the new markup
        return $strMarkup;
    }

    public static function getCategoryLabels($strCategories) {
        $strLabels = '';

        if(!is_array($strCategories)){
            $arrCategories = unserialize($strCategories);
        } else {
            $arrCategories = $strCategories;
        }

        if(isset($arrCategories) && is_array($arrCategories)){

            $arrSortedCategories = array();
            foreach($arrCategories as $arrCategory) {
                $objCategory = CategoryModel::findByPk($arrCategory['category_id']);
                if($objCategory){
                    $arrSortedCategories[$objCategory->id] = $objCategory;
                }
            }

            ksort($arrSortedCategories);

            foreach($arrSortedCategories as $objCategory){

                if($objCategory->color == ''){
                    $objCategory->color = ToolboxService::stringToHexColor($objCategory->title,100,2);
                    $objCategory->save();
                }
                $strTextColor = ToolboxService::getContrastColor($objCategory->color);
                $strLabels .= ' <span style="background-color:#'.$objCategory->color.'; color: '.$strTextColor.';font-size: 0.8em; padding:2px 7px;border-radius: 7px;">' . $objCategory->title . '</span>';

            }
        }

        return $strLabels;
    }
}
