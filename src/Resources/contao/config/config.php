<?php declare(strict_types=1);

/**
 * @package   Memo\MemoFoundationBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Backend CSS
 */

use Contao\System;
use Symfony\Component\HttpFoundation\Request;

if (System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest(System::getContainer()->get('request_stack')->getCurrentRequest() ?? Request::create(''))) {

    // Define CSS File
    $strCSSFileURL = 'bundles/memofoundation/backend.css';
    $strCSSFilePath = 'vendor/memo_development/contao-foundation-bundle/src/Resources/public/backend.css';

    // Get File mtimestamp
    $strRootDir = System::getContainer()->getParameter('kernel.project_dir');
    $strCSSFileTimestamp = filemtime($strRootDir . '/' . $strCSSFilePath);
    $GLOBALS['TL_CSS'][] = $strCSSFileURL . '?v=' . $strCSSFileTimestamp;
}
