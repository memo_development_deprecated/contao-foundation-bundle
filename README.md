# Memo Foundation Bundle

## About

The goal of our foundation-bundle is, to define the "Greatest Common Factor" between all the bundles in use by the Media Motion AG.
This way we can make the topic-bundles (portfolio, team, etc.) lightweight and more standardized.
At the same time we make bugfixing easier, because fixing in in the foundation-bundle or a topic-bundle makes it simple to add the bugfix to all projects by an composer update.

## Table of contents

> * [Memo Foundation Bundle](#title--repository-name)
>   * [About](#about)
>   * [Table of contents](#table-of-contents)
>   * [Installation](#installation)
>   * [Usage](#usage)
>   * [Features](#features)
>   * [Requirements](#requirements)
>   * [Contributing / Reporting issues](#contributing--reporting-issues)
>   * [License](#license)

## Installation

Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo (not on packagist.org) to your composer.json:

```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-foundation-bundle.git"
  },
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-category-bundle.git"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-category-bundle": "^2.0",
"memo_development/contao-foundation-bundle": "^2.0",
```

## Usage
This bundle should only be used in a topic-bundle, as it does not have visible/usable contao features built in.
It only acts as a basis for the topic-bundles.
All topic-bundles should require the foundation-bundle, to use the following features.

## Features
* FoundationModel with Multi-Language Option, getURL() Method, etc.
* Iconset for Multi-Language Option
* Sitemap-Service
* Detail/Listingpage-Service
* Alias-Generator-Service
* Published/Unpublished-Service
* Item-Parser-Service (with Image-Generator)
* Inserttag-Service
* Backend-Rights Handling
* Backend Preview-Service
* Picker-Basic Setup

## Requirements

* PHP 8.1+
* Contao 5.2+

## Contributing / Reporting issues

Bug reports and pull requests are welcome - via Bitbucket-Issues:
[Bitbucket Issues](https://bitbucket.org/memo_development/contao-foundation-bundle/issues?status=new&status=open)

## License

[GNU Lesser General Public License, Version 3.0](https://www.gnu.de/documents/lgpl-3.0.de.html)
